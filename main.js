
(function ($) {
    $.fn.wysiwygEvent = function () {
        return this.each(function () {
            var $this = $(this);
            var htmlOld = $this.html();
            $this.bind('blur keyup paste copy cut mouseup', function () {
                var htmlNew = $this.html();
                if (htmlOld !== htmlNew) {
                    $this.trigger('change');
                    htmlOld = htmlNew;
                }
            })
        })
    }
})(jQuery);

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

class Entry {
    constructor(w, syl, lst) {
        this.word = w;
        this.sylls = syl;
        this.last = lst;
        this.count = syl.length;
    }
}

Exceptions = new Object();
PostExceptions = new Object();
Dictionary = new Object();
SyllLookup = new Object();

Vowels = new Array('a','e','i','o','u','y');
IsVowel = function(c) {
    if(Vowels.indexOf(c) == -1) return false;
    else return true;
}
IsConsonant = function(c) {
    return !IsVowel(c);
};

ProcessLast = function(last) {
    if(Exceptions[last] != null) last = Exceptions[last];
    else {
        while(last.length > 0 && IsConsonant(last[0])) last = last.substr(1);
        while(last.length > 0 && 
            last[last.length-1] == last[last.length-2] && 
            IsConsonant(last[last.length -1])
        ) last = last.slice(0, -1);
//         
        if(PostExceptions[last] != null) last = PostExceptions[last];
    }
    return last;
}

LoadDict = function() {
    $.get( "exceptions.txt", function( data ) {
        var lines = data.split('\n');
        for(var i = 0; i < lines.length; i++) {
            var line = lines[i].trim().toLowerCase();
            if(line.length == 0) continue;
          
            var stage = line[0].trim();
            line = line.substr(1).trim();
            
            var parts = line.split('->');
            
            //console.log(line);
            //console.log(parts.length);
            
            const match = parts[0].trim();
            const replace = parts[1].trim();
            
            //console.log(stage);
            if(stage == "0") Exceptions[match] = replace;
            else if(stage == "1") PostExceptions[match] = replace;
        }
        console.log("Loaded " + lines.length + " exceptions");
        
        //console.log(PostExceptions);
        
        $.get( "syll.txt", function( data ) {
            var lines = data.split('\n');
            for(var i = 0; i < lines.length; i++) {
                const line = lines[i].trim().toLowerCase();
                var sylls = line.split('/');
                
                var word = sylls.join('');
                
                var last = ProcessLast(sylls[sylls.length - 1]);
                
            
                if(last.length == 0) continue;
                Dictionary[word] = new Entry(word, sylls, last);
                
                if(SyllLookup[last] == null) SyllLookup[last] = new Array();
                SyllLookup[last].push(Dictionary[word]);
            }
            console.log("Loaded " + lines.length + " words");
        });
    });
}

GetLineSyllables = function(line) {
    var words = line.trim().split(" ");
    var sylls = 0;
    var tail = "";
    var unknown = false;
    
    for(var i = 0; i < words.length; i++) {
        var word = words[i].toLowerCase();
        if(word.length == 0) continue;
        
        var entry = Dictionary[word];
        if(entry == null && word.length <= 3) {
            Dictionary[word] = new Entry(word, [word], ProcessLast(word));
            entry = Dictionary[word];
        } else if(entry == null && word[word.length-1] == 's') {
            word = word.substr(0, word.length-1);
            if(word[word.length-1] == '\'') word = word.substr(0, word.length-1);
            entry = Dictionary[word];
        }
        
        if(entry == null) {
            unknown = true;
            sylls++;
            continue;
        }
        
        sylls += entry.count;
        if(i == words.length-1) tail = entry.last;
    }
    
    return {rhyme: tail, count:sylls, code:(unknown? "?" : "")};
}

Writer = new Object();
Writer.changed = function() {
    var contents = $("#writer").html().replaceAll("<div>", "\n").replace(/<[^>]+>/g, '').trim();
    var lines = contents.split("\n");
    //console.log(contents);
    var column = "";
    
    for(var i = 0; i < lines.length; i++) {
        var line = lines[i].replace(/[^a-zA-Z ']/g, "").trim();
        var meta = GetLineSyllables(line);
        
        column += "<div style='height:22px;'>" + meta.rhyme + " <span style='float:right;'> "+meta.count+ meta.code +"</span></div>";
    }
    
    $("#lineInfo").html(column);
}

SetupHandlers = function() {
    $("#writer").wysiwygEvent();
    $("#writer").change(function() {
        Writer.changed();
    });
}

$(function() {
    LoadDict();
    SetupHandlers();
});
